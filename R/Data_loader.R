#' data_loader 
#' 
#' Loads RNAseq data from a local directory
#' @param RNAseq_dir the path to load the data from. 
#' @return Returns a list of 3 elements : pData is the correspondance between individuals and group, 
#' WS is the log2 counts per milion data, and ResTable contains logFC, pvalues etc.
#' @details example of RNAseq_dir : 'C:/Users/UserName/Documents/my_RNAseq_dir'
#' @export
data_loader = function(RNAseq_dir){
  
  FILES <- list.files(RNAseq_dir, recursive = T)
  list(ResTable = read.csv2(paste(RNAseq_dir, FILES[grep('ResTable',FILES)], sep = '/')),
       pData = read.csv2(paste(RNAseq_dir, FILES[grep('pData',FILES)], sep = '/'), row.names = 1),
       WS = read.csv2(paste(RNAseq_dir, FILES[grep('WorkingSet_log2CPM',FILES)], sep = '/')))
}

#' duplicated_probes
#' 
#' Identify genes that don't have a unique gene_name identifier in the ResTable
#' @param RNAseq a list created with Data_loader.
#' @returns returns a vector of official gene symbols.
#' @examples
#' duplicated_probes(RNAseq)
#' @export
duplicated_probes <- function(RNAseq)
{
  gene_name = RNAseq$ResTable$gene_name
  tgenes = table(gene_name)
  names(tgenes)[tgenes>1]
}
