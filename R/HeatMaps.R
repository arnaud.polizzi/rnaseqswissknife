#' plot_Venn
#' 
#' plots a Venn diagram
#' @param RNAseq a list created with Data_loader.
#' @param FDR.threshold a threshold to filter the results based on adjusted p.values.
#' @param use.comp can be 'all', 'exclude_interactions', or a vector of comparisons on which FC.threshold and FDR.threshold filtering will be applied.
#' @param FC.threshold a threshold to filter the results based on Fold Change values.
#' @param ggplot Logical: plot the Venn diagram using ggplot
#' @export
plot_Venn <- function(RNAseq,
                      FDR.threshold,
                      use.comp = 'all',
                      FC.threshold = 1,
                      ggplot = TRUE
){
  ResTable = RNAseq$ResTable
  
  comparisons = gsub('logFC_', '',
                     colnames(ResTable)[grepl('logFC',
                                              colnames(ResTable))])
  if (sum(use.comp == 'all') == 1) {
    comparisons = comparisons
  } else if(sum(use.comp == 'exclude_interactions') == 1){
    comparisons = comparisons[sapply(comparisons, function(x){
      sum(grepl('vs', unlist(strsplit(x,'_'))))
    })==1]
  } else {comparisons = use.comp}
  
  if(length(comparisons)>7){
    stop('trying to plot more than 7 comparisons')
  }else{
    logFC.list = as.list(ResTable[, paste('logFC',
                                          comparisons, sep = '_')])
    FDR.list = as.list(ResTable[, paste('FDR',
                                               comparisons, sep = '_')])
    
    Sel.Genes <- mapply(
      function(x, y)
        abs(x) > log2(FC.threshold) & y < FDR.threshold,
      logFC.list,
      FDR.list
    )
    colnames(Sel.Genes) <- comparisons
    lapply(as.list(as.data.frame(Sel.Genes)),
           function(x){
             ResTable$gene_id[x]
           }) |> venn::venn(ggplot = ggplot,
                            ilabels = 'counts')
  }
}

#' meaningful
#'
#' removes genes illogicaly identifed as DEG
#' @param RNAseq a list created with Data_loader.
#' @param min_signif_comparisons number of minimal significant comparison for a gene to be considered differentialy expressed.
#' @param FDR.threshold a threshold to filter the results based on adjusted p.values.
#' @param use.comp can be 'all', 'exclude_interactions', or a vector of comparisons tho which FC.threshold and FDR.threshold will be applied.
#' @param FC.threshold a threshold to filter the results based on Fold Change values.
#' @param exclude.group a vector of groups to exclude, as written in pData$group.
#' @param exclude.comp a vector of comparions to exclude, as written in the column names of ResTable.
#' @returns a vector of genes considered as differentialy expressed. Can be used as sel.genes argument in the plot_HM function.
#' @export
meaningful <- function(RNAseq,
                       min_signif_comparisons,
                       FDR.threshold,
                       FC.threshold = 1,
                       exclude.group = 'none',
                       exclude.comp = 'none'){
  if(sum(exclude.group == 'none') == 1){
    ResTable = RNAseq$ResTable
  }else{
    ResTable = RNAseq$ResTable[,!grepl(paste(exclude.group, collapse = '|'),
                                       colnames(RNAseq$ResTable))]
  }
  
  if(sum(exclude.comp == 'none') == 1){
    ResTable = ResTable
  }else{
    ResTable = ResTable[,!grepl(paste(exclude.comp, collapse = '|'),
                                colnames(ResTable))]
  }  
  
  comparisons = gsub('logFC_', '',
                     colnames(ResTable)[grepl('logFC',
                                              colnames(ResTable))])
  
  logFC.list = as.list(ResTable[, paste('logFC',
                                        comparisons, sep = '_')])
  FDR.list = as.list(ResTable[, paste('FDR',
                                             comparisons, sep = '_')])
  
  Sel.Genes <- ResTable$gene_id[mapply(
    function(x, y)
      abs(x) > log2(FC.threshold) & y < FDR.threshold,
    logFC.list,
    FDR.list
  ) |>
    rowSums() >= min_signif_comparisons]
  
  Sel.Genes
}

#' plot_HM
#' 
#' Plots a heat map
#' @param RNAseq a list created with Data_loader.
#' @param HMcolors a vector of colors for heatmap contrast.
#' @param FDR.threshold a threshold to filter the results based on adjusted p.values.
#' @param sel.genes a vector of genes to plot, named by their ENSEMBL identifier.
#' @param use.comp a vector of comparisons tho which FC.threshold and FDR.threshold will be applied.
#' @param FC.threshold a threshold to filter the results based on Fold Change values.
#' @param group_colors a vector of colors for the groups.
#' @param show_rownames whether to plot the names of the genes or not.
#' @param fontzise_col size of the font for group names.
#' @param remove.group a vector of goup names to remove, as writen in pData$group.
#' @param remove.ind a vector of indiviuals to remove, as writen in the row names of pData.
#' @param ClusTableName the name of the output table of the heatmap.
#' @returns returns a heatmap as a ggplot, and a table summarizing the genes by cluster.
#' @examples
#' plot_HM(CuratedRNAseq,
#' nclust = 6,
#' use.comp = 'All',
#' FDR.threshold = .05)
#' @export
plot_HM <- function(RNAseq,
                    HMcolors = c("blue", "blue4", "yellow4", "yellow"),
                    nclust,
                    FDR.threshold,
                    sel.genes = 'all',
                    use.comp = 'all',
                    FC.threshold = 1,
                    group_colors = NULL,
                    show_rownames = FALSE,
                    fontsize_col = 3,
                    remove.group = 'none',
                    remove.ind = 'none',
                    ClusTableName = 'HMclust'
) {
  if(sum(sel.genes == 'all') == 1){
    ResTable = RNAseq$ResTable
    WS = RNAseq$WS
  }else{
    ResTable = RNAseq$ResTable[RNAseq$ResTable$gene_id %in% sel.genes,]
    WS = RNAseq$WS[RNAseq$ResTable$gene_id %in% sel.genes,]
  }
  
  if (sum(remove.group == 'none') == 1)
  {
    pData = data.frame(group = RNAseq$pData,
                       row.names = row.names(RNAseq$pData))
  } else {
    pData = data.frame(group = RNAseq$pData[!RNAseq$pData$group %in% remove.group,],
                       row.names = row.names(RNAseq$pData)[!RNAseq$pData$group %in% remove.group])
  }
  
  if (sum(remove.ind == 'none') == 1)
  {
    pData = pData
  } else {
    pData = data.frame(group = pData$group[!row.names(pData) %in% remove.ind],
                       row.names = row.names(pData)[!row.names(pData) %in% remove.ind])
  }
  
  if (is.null(group_colors)) {
    group_colors = scales::viridis_pal()(length(unique(pData$group)))
    names(group_colors) <- unique(pData$group)
  }
  else{
    group_colors = group_colors
    names(group_colors) <- unique(pData$group)
  }
  
  comparisons = gsub('logFC_', '',
                     colnames(ResTable)[grepl('logFC',
                                              colnames(ResTable))])
  if (sum(use.comp == 'all') == 1) {
    comparisons = comparisons
  } else{
    comparisons = use.comp
  }
  
  logFC.list = as.list(ResTable[, paste('logFC',
                                        comparisons, sep = '_')])
  FDR.list = as.list(ResTable[, paste('FDR',
                                      comparisons, sep = '_')])
  
  Sel.Genes <- ResTable$gene_id[mapply(
    function(x, y)
      abs(x) > log2(FC.threshold) & y < FDR.threshold,
    logFC.list,
    FDR.list
  ) |>
    rowSums() > 0]
  
  dataHM <- WS[ResTable$gene_id %in% Sel.Genes,
               row.names(pData)]
  row.names(dataHM) <- Sel.Genes
  colnames(dataHM) <- paste(pData$group, row.names(pData), sep = '.')
  
  # Pair correlate columns (samples)
  cols.cor <- cor(dataHM,
                  use = "pairwise.complete.obs",
                  method = 'pearson')
  # Pair correlate  lines (genes)
  rows.cor <- cor(t(dataHM),
                  use = "pairwise.complete.obs",
                  method = 'pearson')
  
  # first, do HM without row clustering
  pres <- pheatmap::pheatmap(dataHM,
                             scale = "row", 
                             color = colorRampPalette(HMcolors)(50),
                             clustering_distance_cols = as.dist(1 - cols.cor),
                             clustering_distance_rows =as.dist(1 - rows.cor),
                             clustering_method = "ward.D2",
                             show_rownames = FALSE,
                             silent = T)
  
  
  my_gene_col <- data.frame(cluster = as.numeric(factor(cutree(tree = pres$tree_row,
                                                               k = nclust)[pres$tree_row$order],
                                                        levels = unique(cutree(tree = pres$tree_row,
                                                                               k = nclust)[pres$tree_row$order]),
                                                        ordered = T)),
                            row.names = pres$tree_row$labels[pres$tree_row$order])
  
  my_sample_col <- data.frame(sample = pData$group,
                              row.names = colnames(dataHM)) 
  
  clustcol <- list(sample = group_colors,
                   cluster = setNames(rep(c("grey20","grey80"),
                                          (nclust/2)+1)[1:nclust],
                                      1:nclust))
  
  pHM<- pheatmap::pheatmap(dataHM,
                           scale = "row", 
                           color = colorRampPalette(HMcolors)(50),
                           clustering_distance_cols = as.dist(1 - cols.cor),
                           clustering_distance_rows =as.dist(1 - rows.cor),
                           clustering_method = "ward.D2",
                           annotation_colors = clustcol,
                           annotation_col = my_sample_col,
                           annotation_row = my_gene_col,
                           show_rownames = show_rownames,
                           legend = FALSE,
                           annotation_legend = FALSE,
                           treeheight_row = 10,
                           treeheight_col = 10,
                           fontisize = 3,
                           fontsize_col = fontsize_col,
                           silent = T)
  
  shrinkResTable <- ResTable[ResTable$gene_id %in%
                               row.names(my_gene_col),]
  clusteredResTable <- cbind.data.frame(
    cluster = my_gene_col$cluster,
    shrinkResTable[
      match(row.names(my_gene_col),
            shrinkResTable$gene_id[shrinkResTable$gene_id %in%
                                     row.names(my_gene_col)]),
    ])
  clusteredResTable <- clusteredResTable[order(clusteredResTable$cluster),]
  assign(ClusTableName,
         list(ResTable = clusteredResTable,
              pData = pData,
              dataHM = dataHM[match(clusteredResTable$gene_id,
                                    row.names(dataHM)),]),
         envir=.GlobalEnv
  )
  
  return(ggpubr::ggarrange(pHM[[4]]))
}

#' cluster_profiles
#' 
#' plots the profile of clusters generated with plot_HM
#' 
#' @export
cluster_profiles <- function(resHM,
                             name_factor1,
                             name_factor2 = NULL,
                             lvls_factor1,
                             lvls_factor2 = NULL,
                             color_factor1 = NULL){
  NormData <- as.data.frame(t(apply(resHM$dataHM, 1, function(x){(x-mean(x))/sd(x)})))
  
  if(!is.null(name_factor2)){
    dataPlot <-  cbind.data.frame(
      group = rep(resHM$pData$group, each = dim(NormData)[1]),
      GeneExpression = do.call(
        rbind,
        lapply(as.list(as.data.frame((
          t(NormData[match(resHM$ResTable$gene_id,
                           row.names(NormData)),])
        ))),
        function(y) {
          sapply(unique(resHM$pData$group),
                 function(x) {
                   mean(y[resHM$pData$group == x])
                 })
        }) |>
          as.data.frame() |>
          t() |> 
          as.list()
      ),
      resHM$ResTable[, c('gene_id', 'cluster')]
    ) 
    dataPlot <-  cbind.data.frame(dataPlot,
                                  t(as.data.frame(strsplit(dataPlot$group, '_'))))
    row.names(dataPlot) <- 1:dim(dataPlot)[1]
    if(sum(lvls_factor1 %in% dataPlot[,5]) > 0){dataPlot = dataPlot}else{
      dataPlot[,7] <- dataPlot[,5]
      dataPlot <- dataPlot[,-5]
    }
    
    colnames(dataPlot) <- c(
      'group', 
      'GeneExpression', 
      'gene_id', 
      'cluster',
      ifelse(sum(lvls_factor1 %in% dataPlot[,5])>0, name_factor1, name_factor2),
      ifelse(sum(lvls_factor1 %in% dataPlot[,5])>0, name_factor2, name_factor1)
    )
    
    dataPlot[,name_factor1] <- factor(dataPlot[,name_factor1],
                                      levels = lvls_factor1,
                                      ordered = T)
    dataPlot[,name_factor2] <- factor(dataPlot[,name_factor2],
                                      levels = lvls_factor2,
                                      ordered = T)
    
    dataLines <- cbind.data.frame(
      group = rep(unique(dataPlot$group), each = max(dataPlot$cluster)),
      cluster = rep(unique(dataPlot$cluster), length(unique(dataPlot$group))),
      GeneExpression = sapply(unique(paste(dataPlot$group, dataPlot$cluster, sep = '.')),
                              function(x){mean(dataPlot$GeneExpression[paste(dataPlot$group, dataPlot$cluster, sep = '.') == x])})
    )
    dataLines <- cbind.data.frame(
      dataLines,
      t(as.data.frame(sapply(dataLines$group, function(x){strsplit(x, '_')})))
    )
    
    colnames(dataLines) = c(colnames(dataLines)[1:3],
                            ifelse(sum(lvls_factor1 %in% dataLines[,4])>0, name_factor1, name_factor2),
                            ifelse(sum(lvls_factor1 %in% dataLines[,4])>0, name_factor2, name_factor1))
    dataLines[,name_factor1] <- factor(dataLines[,name_factor1],
                                       levels = lvls_factor1,
                                       ordered = T)
    dataLines[,name_factor2] <- factor(dataLines[,name_factor2],
                                       levels = lvls_factor2,
                                       ordered = T)
    
    dataPlot |> 
      ggplot(aes(x = !!sym(name_factor1),
                 y = GeneExpression,
                 color = !!sym(name_factor2)))+
      geom_line(data = dataLines,
                aes(x = !!sym(name_factor1), 
                    y = GeneExpression, 
                    linetype = !!sym(name_factor2),
                    group = !!sym(name_factor2)))+
      ylab('log-centered gene expression') +
      stat_summary(fun.data = mean_cl_normal,
                   aes(color = !!sym(name_factor2)),
                   size = .1,
                   na.rm = TRUE)+
      facet_wrap(facets = vars(cluster), scales = 'free_y', ncol = 1)
    
  }
  
}


